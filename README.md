
# Top Ada library package

for all components published by maintainer `Sterna-Marine`.

Those components are OpenSource Apache licenced.

# Common AdaForge library structure

* **Folder structure** `./bld` `./src/main` `./src/lib` `./src/demo` `./tests/src/main` `./tests/src/testsuite`
* **Filenames/Package names** refactoring to reflect [AdaForge's Component/Crates catalog](https://www.adaforge.org/Libraries/)
* **Build scripts** have been adapted to be full compatible with both of those build systems:
   * [ALIRE](https://alire.ada.dev/docs/) (`alr`)
   * [GNAT Adacore GPR](https://docs.adacore.com/live/wave/gprbuild/html/gprbuild_ug/gprbuild_ug.html) (`gprbuild`)
* **Unit tests** have been ported to an xUnit compatible framework as to be run through the `AdaForge.DevTools.TestTools.UnitTests` framework (port of the excellent [AHVEN AUnit framework](http://ahven.stronglytyped.org/)).
